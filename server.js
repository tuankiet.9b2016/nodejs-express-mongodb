const express = require("express");
const cors = require("cors");

const app = express();

var corsOptions = {
    origin: "https://localhost:8081"
};

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({extended: true}));

require("./app/routes/tutorial.routes")(app);

app.get('/', (req,res) => {
    
    res.json({message: "Welcome to nodejs-mongodb!"});
})

const PORT = process.env.PORT || 8080;
app.listen(PORT,() => {
    console.log(`Server is running at ${PORT}`);
});

const db = require("./app/models");
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch(err => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });
